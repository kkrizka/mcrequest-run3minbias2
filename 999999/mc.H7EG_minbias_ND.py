## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "LHC-MB"
evgenConfig.description = "MinBias with H7.2"
evgenConfig.keywords = ["QCD", "jets", "minBias"]
evgenConfig.contact  = [ "Karol Krizka <karol.krizka@cern.ch>" ]
evgenConfig.nEventsPerJob = 50000

## Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")
include("Herwig7_i/Herwig71_EvtGen.py")

# this follows $HERWIG7_PATH/share/Herwig/LHC-MB.in
# see also Jose's QT report https://cds.cern.ch/record/2753482/
command = """
set /Herwig/Shower/ShowerHandler:IntrinsicPtGaussian 2.2*GeV
read snippets/MB.in
"""
#read snippets/Diffraction.in

print command

Herwig7Config.add_commands(command)

## run the generator
Herwig7Config.run()
